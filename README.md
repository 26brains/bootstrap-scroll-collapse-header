# README #

This is a simple example demonstrating a way to create a combination fixed and collapsable header together with an initially dynamic and then fixed navigation.

The use case is as follows:

1. A fixed header at the top of the page. The fixed header does not move when the page scrolls.
1. A collapsable header, beneath the fixed header, that scrolls up at the same rate as the rest of the page as the page scrolls.
1. A nav bar, beneath the collapsable header, that scrolls up at the same rate as the rest of the page, but then locks to the bottom of the fixed header when the page scroll reaches far enough.
1. Content beneath the nav bar that scrolls as normal.

The repo is just HTML, CSS and JS. It comes with Bootstrap CSS and JS.